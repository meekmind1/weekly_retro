var app5 = new Vue({
  el: '#app',
  data: {
    message: 'Hello!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})
