const express = require('express');
const exphbs = require('express-handlebars');
const app = express();

//Register Handlebars view engine
app.engine('.hbs', exphbs({
  defaultLayout: 'template',
  extname: '.hbs',
  partialsDir: ['views/partials/']
}));
//use Handlebars view engine
app.set('view engine', '.hbs');

app.use(express.static('stylesheets'));
app.use(express.static('javascript'));

app.get('/', (req, res) => {
  res.render('index');
});

app.listen(3000, () => {
  console.log('iniciado en PORT 3000');
});
