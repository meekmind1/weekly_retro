new Vue({
    el: '#app',
    data: { 
      books:[{
        Name:'Book1',
        Picture:'https://orion-uploads.openroadmedia.com/lg_cd3e01731957-lord-of-the-flies.jpg',
        alt:'book1'},

        {
        Name:'Book2',
        Picture:'https://i.pinimg.com/originals/e8/93/f6/e893f6f43b40076f08b73aa9f4761d5d.jpg',
        alt:'book2'},

        {
        Name:'Book3',
        Picture:'http://www.readersdigest.ca/wp-content/uploads/2017/03/classic-books-the-great-gatsby.jpg',
        alt:'book3'},

        {
        Name:'Book4',
        Picture:'https://shortlist.imgix.net/app/uploads/2016/06/24222744/13-famous-books-that-had-very-different-working-titles-8.jpg?w=1200&h=1&fit=max&auto=format%2Ccompress',
        alt:'book4'},

        {
        Name:'Book5',
        Picture:'http://ka-writing.com/wp-content/uploads/2014/09/gonewind.jpg',
        alt:'book5'},

        {
        Name:'Book6',
        Picture:'https://imgix.ranker.com/node_img/78/1559202/original/mein-kampf-books-photo-1?w=650&q=50&fm=jpg&fit=fill&bg=fff',
        alt:'book6'},
        {
        Name:'Book7',
        Picture:'https://i.pinimg.com/originals/5a/9b/b0/5a9bb0b9ada80d6d4647ace23ba11e65.jpg',
        alt:'book7'},

        {
        Name:'Book8',
        Picture:'https://dennybradburybooks.files.wordpress.com/2011/05/the-witches.jpg',
        alt:'book8'},

        {
        Name:'Book9',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book9'},

        {
        Name:'Book10',
        Picture:'https://i.pinimg.com/originals/57/56/b7/5756b7df90f18ce1536a3c81e81ce5c7.jpg',
        alt:'book10'},

        {
        Name:'Book11',
        Picture:'http://flavorwire.files.wordpress.com/2013/04/prince.jpg',
        alt:'book1'},

        {
        Name:'Book12',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
        
        {
        Name:'Book13',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book14',
        Picture:'https://www.whsmith.co.uk/pws/client/images/catalogue/products/9781/44/4929706/xlarge/9781444929706_1.jpg',
        alt:'book1'},

        {
        Name:'Book15',
        Picture:'https://www.bookstr.com/sites/default/files/inline-images/51h8aHlS%2BzL.jpg',
        alt:'book1'},
    
        {
        Name:'Book16',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/51ioMI8LoTL.jpg',
        alt:'book1'},

        {
        Name:'Book17',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/51lbRkLxyoL._SY445_.jpg',
        alt:'book1'},

        {
        Name:'Book18',
        Picture:'https://i0.wp.com/www.dawzz.com/wp-content/uploads/2016/04/The-Girl-in-the-Ice-A-gripping-serial-killer-thriller-Detective-Erika-Foster-crime-thriller-novel-Volume-1-0.jpg?resize=324%2C500',
        alt:'book1'},

        {
        Name:'Book19',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book20',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book21',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book22',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
        {
        Name:'Book23',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book24',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book25',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book26',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book27',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book28',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
        
        {
        Name:'Book29',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book30',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book31',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
    
        {
        Name:'Book32',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},      

        {
        Name:'Book33',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book34',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book35',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book36',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book37',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book38',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
        {
        Name:'Book39',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book40',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book41',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book42',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book43',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book44',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
        
        {
        Name:'Book45',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book46',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},

        {
        Name:'Book47',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'},
    
        {
        Name:'Book48',
        Picture:'https://images-na.ssl-images-amazon.com/images/I/A1WGSvMSjvL._AC_SY230_.jpg',
        alt:'book1'}    
    
      ],
      items : 0
    },
    methods: {
        addCart: function() {
          this.items++;
          var itemsCart = document.getElementById('items-cart');
          itemsCart.innerText = this.items + '   Item(s)';
        },
        showInfo: function(event) {
                var Picture = event.target.src;
                var imgModal = document.querySelector('#img-modal');
                imgModal.src = Picture;
                var Name = event.target;
                var bookName = document.querySelector('#book-Name');
                bookName = Name;
            },
        
    }
  
  })
